//
//  CryptoMarketDataClient.swift
//  Apophis
//
//  Created by Maxim Tzvetkov on 6.02.19.
//  Copyright © 2019 Jericho Inc. All rights reserved.
//

import Foundation

public class CryptoMarketDataService: ServiceClient<[CryptoCoinMarketData]> {
    
    public override init() {
    }
    
    // MARK: ServiceClient
    public override var path: String {
        return "v1/assets"
    }
    
    public override var httpMethod: HTTPMethod {
        return .get
    }
    
    public override var task: HTTPTask {
        return .request
    }
}

extension API {
    
    func retrieveMarketData(_ completion: @escaping (APIRequestResult<[CryptoCoinMarketData]>) -> Void) {
        
        let service = CryptoMarketDataService()
        
        service.request { result in
            switch result {
            case .success(let data):
                completion(APIRequestResult.success(data: data))
            case .failure(let error):
                completion(APIRequestResult.failure(errorMessage: "", error: error))
            }
        }
    }
}
