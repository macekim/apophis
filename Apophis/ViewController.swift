//
//  ViewController.swift
//  Apophis
//
//  Created by Maxim Tzvetkov on 12/9/18.
//  Copyright © 2018 Jericho Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet private weak var tableView: UITableView!
    
    var aliases = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        API.shared.retrieveMarketData { result in
            switch result {
            case .success(let data):
                data.forEach({ element in
                    print(element)
                })
            case .failure(let errorMSg, _):
                print(errorMSg)
            }
        }
        
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aliases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel?.text = aliases[indexPath.row]
        
        return cell
    }
}
