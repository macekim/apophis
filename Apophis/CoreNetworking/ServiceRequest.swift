//
//  ServiceRequest.swift
//  Apophis
//
//  Created by Maxim Tzvetkov on 4.02.19.
//  Copyright © 2019 Jericho Inc. All rights reserved.
//

import Foundation

public class ServiceClient<CodableType: Codable>: EndpointType, APIClient {
    
    public var path: String {
        return ""
    }
    
    public var httpMethod: HTTPMethod {
        return .get
    }
    
    public var task: HTTPTask {
        return .request
    }
    
    public var headers: HTTPHeaders? {
        return nil
    }
    
    public init () {}
    
    public var configuration = APHConfiguration()
}

extension ServiceClient {
    
    func request(_ completion: @escaping (Result<CodableType, APIError>) -> Void) {
        fetch(with: request, decode: { result -> CodableType? in
            guard let result = result as? CodableType else { return nil }
            return result
        }, completion: completion)
    }
    
    public func buildRequest() throws -> URLRequest {
        
        var request: URLRequest!
        
        if let url = URL(string: self.configuration.baseUrl + "/" + self.path) {
            
            request = URLRequest(
                url: url,
                cachePolicy: self.configuration.urlRequestCachePolicy,
                timeoutInterval: self.configuration.requestTimeout)
            
            request.httpMethod = self.httpMethod.rawValue
            
            do {
                switch self.task {
                case .request:
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    
                case .requestParams(let bodyParams, let urlParams, let encoding):
                    try self.configureParameters(
                        bodyParams: bodyParams,
                        urlParams: urlParams,
                        encoding: encoding,
                        request: &request)
                    
                case .requestParamsAndHeaders(let bodyParams, let urlParams, let encoding, let additionalHeaders):
                    self.addAdditionalHeader(additionalHeaders, request: &request)
                    
                    try self.configureParameters(
                        bodyParams: bodyParams,
                        urlParams: urlParams,
                        encoding: encoding,
                        request: &request)
                }
                
            } catch {
                throw error
            }
        }
        
        return request
    }

    private var request: URLRequest {
        return try! self.buildRequest()
    }
    
    // Parameters for url and json
    private func configureParameters(
        bodyParams: Parameters?,
        urlParams: Parameters?,
        encoding: ParameterEncoding,
        request: inout URLRequest) throws {
        
        do {
            try encoding.encode(
                request: &request,
                bodyParameters: bodyParams,
                urlParameters: urlParams)
        } catch {
            throw error
        }
    }
    
    private func addAdditionalHeader(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        
        guard let headers = additionalHeaders else { return }
        
        headers.forEach { key, value in
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
}
