//
//  NetworkConfiguration.swift
//  Apophis
//
//  Created by Maxim Tzvetkov on 5.02.19.
//  Copyright © 2019 Jericho Inc. All rights reserved.
//

import Foundation

public class APHConfiguration {
    
    public var logginEnabled = true
    public var logLevel = LogLevel.info
    
    public var baseUrl = "https://rest.coinapi.io"
    public var apiKey = "40BC0824-DCDC-448A-A31E-197E7048C421"
    public var urlRequestCachePolicy: URLRequest.CachePolicy = URLRequest.CachePolicy.useProtocolCachePolicy
    public var requestTimeout: Double = 30.0
    
    public var cookieAcceptPolicy = HTTPCookie.AcceptPolicy.onlyFromMainDocumentDomain
    public var sessionCookieName = "sessionId"
    
    // override point
    public var session: URLSession = {
        return URLSession(configuration: .default)
    }()
    
    public lazy var sessionConfig: URLSessionConfiguration = {
        
        let configuration = URLSessionConfiguration.ephemeral
        
        configuration.httpCookieAcceptPolicy = cookieAcceptPolicy
        configuration.httpShouldSetCookies = false
        
        return configuration
    }()
    
    public init(config: APHConfiguration) {
        self.logginEnabled = config.logginEnabled
        self.logLevel = config.logLevel
    }
    
    public init() {}
}
