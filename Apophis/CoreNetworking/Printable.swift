//
//  Printable.swift
//  Apophis
//
//  Created by Maxim Tzvetkov on 5.02.19.
//  Copyright © 2019 Jericho Inc. All rights reserved.
//

import Foundation

public protocol ErrorCodable: Codable {
    func hasValidErrorResponse() -> Bool
}

public protocol ServicePrintable {
    func printRequestInfo(_ emptyLines: Bool, config: APHConfiguration, requestHeaders: HTTPHeaders?)
    func printResponseInfo(_ response: HTTPURLResponse, responseString: String?, config: APHConfiguration)
}

public enum APIRequestResult<T> {
    case success(data: T)
    case failure(errorMessage: String?, error: Error)
}

public enum RequestResult<T: Codable, E: ErrorCodable> {
    case success(statusCode: Int, data: T)
    case failure(statusCode: Int, errorBody: E?, error: Error)
    case failureWithoutResponse(error: Error)
    case inMaintenance(errorBody: E?, error: Error)
}
