//
//  Http.swift
//  Apophis
//
//  Created by Maxim Tzvetkov on 1.02.19.
//  Copyright © 2019 Jericho Inc. All rights reserved.
//

import Foundation

public typealias Parameters = [String: Any]
public typealias HTTPHeaders = [String: String]

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}

public enum HTTPTask {
    case request
    
    case requestParams(
        bodyParams: Parameters?,
        urlParams: Parameters?,
        encoding: ParameterEncoding)
    
    case requestParamsAndHeaders(
        bodyParams: Parameters?,
        urlParams: Parameters?,
        encoding: ParameterEncoding,
        additionalHeaders: HTTPHeaders?)
}

public protocol EndpointType {
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}
