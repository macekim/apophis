//
//  InfoLogger.swift
//  Apophis
//
//  Created by Maxim Tzvetkov on 5.02.19.
//  Copyright © 2019 Jericho Inc. All rights reserved.
//

import Foundation

public enum LogLevel: Int {
    case error
    case warning
    case success
    case info
    case custom
    case bug
    case debug
}

public class Logger {
    
    static let shared = Logger()
    
    var level: LogLevel = .custom
    var enables: Bool = true
    
    private static let errorEmoji: String = "🚫"
    private static let warningEmoji: String = "⚠️"
    private static let successEmoji: String = "✅"
    private static let infoEmoji: String = "ℹ️"
    private static let customEmoji: String = "🔶"
    private static let bugEmoji: String = "🐛"
    private static let debugEmoji: String = "🔧"
    
    public class func log(_ message: String, logLevel: LogLevel) {
        
        let configuration = APHConfiguration()
        
        if configuration.logginEnabled {
            if logLevel.rawValue <= configuration.logLevel.rawValue {
                Logger.printLevel(message, logLevel: logLevel)
            }
        }
    }
    
    public class func debug(_ message: String) {
        Logger.log(message, logLevel: LogLevel.debug)
    }
    
    public class func info (_ message: String) {
        Logger.log(message, logLevel: LogLevel.info)
    }
    
    public class func warning (_ message: String) {
        Logger.log(message, logLevel: LogLevel.warning)
    }
    
    public class func error (_ message: String) {
        Logger.log(message, logLevel: LogLevel.error)
    }
    
    // MARK: Private methods
    private class func printLevel (_ message: String, logLevel: LogLevel) {
        var output: String
        
        if message == "" {
            print(message)
            return
        }
        
        let timestamp = Logger.getTimestamp()
        
        switch logLevel {
        case LogLevel.error:
            output = "\(Logger.getEmojiForLogLevel(logLevel)) [\(timestamp)] \(message)"
        case LogLevel.warning:
            output = "\(Logger.getEmojiForLogLevel(logLevel)) [\(timestamp)] \(message)"
        case LogLevel.success:
            output = "\(Logger.getEmojiForLogLevel(logLevel)) [\(timestamp)] \(message)"
        case LogLevel.info:
            output = "\(Logger.getEmojiForLogLevel(logLevel)) [\(timestamp)] \(message)"
        case LogLevel.custom:
            output = "\(Logger.getEmojiForLogLevel(logLevel)) [\(timestamp)] \(message)"
        case LogLevel.bug:
            output = "\(Logger.getEmojiForLogLevel(logLevel)) [\(timestamp)] \(message)"
        case LogLevel.debug:
            output = "\(Logger.getEmojiForLogLevel(logLevel)) [\(timestamp)] \(message)"
            
        }
        print(output)
    }
    
    private class func getEmojiForLogLevel(_ logLevel: LogLevel) -> String {
        
        switch logLevel {
        case .error:
            return "\(Logger.errorEmoji)"
        case .warning:
            return "\(Logger.warningEmoji)"
        case .success:
            return "\(Logger.successEmoji)"
        case .info:
            return "\(Logger.infoEmoji)"
        case .custom:
            return "\(Logger.customEmoji)"
        case .bug:
            return "\(Logger.bugEmoji)"
        case .debug:
            return "\(Logger.debugEmoji)"
        }
    }
    
    private static let stampDateFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss.SSS"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    
    private class func getTimestamp() -> String {
        return Logger.stampDateFormatter.string(from: Date())
    }
}
