//
//  AppDriver.swift
//  Apophis
//
//  Created by Maxim Tzvetkov on 13.02.19.
//  Copyright © 2019 Jericho Inc. All rights reserved.
//

import Foundation
import RxSwift

public class API {
    
    public static let shared: API = API()
    
    public var appDisplayVersion = "" // somethin like 1.0.0
    public var appBuildNumber = 0
    
    public var configuration = APHConfiguration()
}
