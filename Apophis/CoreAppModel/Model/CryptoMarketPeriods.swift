/*
 Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation

struct CryptoMarketPeriods : Decodable {
    let period_id : String?
    let length_seconds : Int?
    let length_months : Int?
    let unit_count : Int?
    let unit_name : String?
    let display_name : String?
    
    enum CodingKeys: String, CodingKey {
        
        case period_id = "period_id"
        case length_seconds = "length_seconds"
        case length_months = "length_months"
        case unit_count = "unit_count"
        case unit_name = "unit_name"
        case display_name = "display_name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        period_id = try values.decodeIfPresent(String.self, forKey: .period_id)
        length_seconds = try values.decodeIfPresent(Int.self, forKey: .length_seconds)
        length_months = try values.decodeIfPresent(Int.self, forKey: .length_months)
        unit_count = try values.decodeIfPresent(Int.self, forKey: .unit_count)
        unit_name = try values.decodeIfPresent(String.self, forKey: .unit_name)
        display_name = try values.decodeIfPresent(String.self, forKey: .display_name)
    }
}
