/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

public struct CryptoCoinMarketData : Codable {
    
	let asset_id : String?
	let name : String?
	let type_is_crypto : Int?
	let data_start : String?
	let data_end : String?
	let data_quote_start : String?
	let data_quote_end : String?
	let data_orderbook_start : String?
	let data_orderbook_end : String?
	let data_trade_start : String?
	let data_trade_end : String?
	let data_trade_count : Int?
	let data_symbols_count : Int?

	enum CodingKeys: String, CodingKey {

		case asset_id = "asset_id"
		case name = "name"
		case type_is_crypto = "type_is_crypto"
		case data_start = "data_start"
		case data_end = "data_end"
		case data_quote_start = "data_quote_start"
		case data_quote_end = "data_quote_end"
		case data_orderbook_start = "data_orderbook_start"
		case data_orderbook_end = "data_orderbook_end"
		case data_trade_start = "data_trade_start"
		case data_trade_end = "data_trade_end"
		case data_trade_count = "data_trade_count"
		case data_symbols_count = "data_symbols_count"
	}

    public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		asset_id = try values.decodeIfPresent(String.self, forKey: .asset_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		type_is_crypto = try values.decodeIfPresent(Int.self, forKey: .type_is_crypto)
		data_start = try values.decodeIfPresent(String.self, forKey: .data_start)
		data_end = try values.decodeIfPresent(String.self, forKey: .data_end)
		data_quote_start = try values.decodeIfPresent(String.self, forKey: .data_quote_start)
		data_quote_end = try values.decodeIfPresent(String.self, forKey: .data_quote_end)
		data_orderbook_start = try values.decodeIfPresent(String.self, forKey: .data_orderbook_start)
		data_orderbook_end = try values.decodeIfPresent(String.self, forKey: .data_orderbook_end)
		data_trade_start = try values.decodeIfPresent(String.self, forKey: .data_trade_start)
		data_trade_end = try values.decodeIfPresent(String.self, forKey: .data_trade_end)
		data_trade_count = try values.decodeIfPresent(Int.self, forKey: .data_trade_count)
		data_symbols_count = try values.decodeIfPresent(Int.self, forKey: .data_symbols_count)
	}
}
