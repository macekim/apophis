//
//  ViewModelBased.swift
//  Apophis
//
//  Created by Maxim Tzvetkov on 5.02.19.
//  Copyright © 2019 Jericho Inc. All rights reserved.
//

import UIKit

public protocol StoryboardBased: class {
    static var storyboard: UIStoryboard { get }
}

public extension StoryboardBased {
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: String(describing: self), bundle: Bundle(for: self))
    }
}

public extension StoryboardBased where Self: UIViewController {
    
    static func instantiate() -> Self {
        guard let viewController = storyboard.instantiateInitialViewController() as? Self else {
            fatalError("Could not instantiate viewController of \(self)")
        }
        
        return viewController
    }
}

// Generic ViewModel definition: ViewControllers must conform to this protocol
public protocol ViewModelBased: class {
    
    associatedtype GenericViewModel
    associatedtype GenericStyle
    
    var viewModel: GenericViewModel! { get set }
    var style: GenericStyle! { get set }
}

public extension ViewModelBased where Self: StoryboardBased & UIViewController {
    
    static func instantiate(with viewModel: GenericViewModel, and style: GenericStyle) -> Self {
        let viewController = Self.instantiate()
        viewController.viewModel = viewModel
        viewController.style = style
        
        return viewController
    }
}
