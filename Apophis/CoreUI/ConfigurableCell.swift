//
//  ConfigurableCell.swift
//  Apophis
//
//  Created by Maxim Tzvetkov on 5.02.19.
//  Copyright © 2019 Jericho Inc. All rights reserved.
//

import UIKit

public protocol ConfigurableCell {
    associatedtype DataType
    func configure(data: DataType)
}

public protocol CellConfigurator {
    static var reuseIdentifier: String { get }
    func configure(cell: UIView)
}

public class TableCellConfigurator<CellType: ConfigurableCell, DataType>: CellConfigurator where CellType.DataType == DataType, CellType: UITableViewCell {
    
    private let item: DataType
    
    public init(item: DataType) {
        self.item = item
    }
    
    public static var reuseIdentifier: String {
        return String(describing: CellType.self)
    }
    
    public func configure(cell: UIView) {
        (cell as! CellType).configure(data: item)
    }
}
